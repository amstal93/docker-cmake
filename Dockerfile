# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION} as alpine

FROM alpine as builder

ARG CMAKE_VERSION=3.15.2

# Download, configure, build and install the Clang C/C++ compiler
RUN apk add --quiet --no-cache \
        linux-headers \
        build-base \
        make \
        git \
        gcc \
        g++ && \
    git clone \
        --branch v${CMAKE_VERSION} \
        --single-branch \
        --depth 1 \
        https://github.com/Kitware/CMake.git && \
    cd CMake && \
    ./bootstrap -- -DCMAKE_BUILD_TYPE:STRING=Release && \
    make --jobs=$(nproc) && \
    make --jobs=$(nproc) install && \
    cmake --version

FROM alpine

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

# Build-time metadata as defined at http://label-schema.org
LABEL \
    maintainer="tymoteusz.blazejczyk.pl@gmail.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="tymonx/cmake" \
    org.label-schema.description="A Docker image with the CMake" \
    org.label-schema.usage="https://gitlab.com/tymonx/docker-cmake/README.md" \
    org.label-schema.url="https://gitlab.com/tymonx/docker-cmake" \
    org.label-schema.vcs-url="https://gitlab.com/tymonx/docker-cmake" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vendor="tymonx" \
    org.label-schema.version=$VERSION \
    org.label-schema.docker.cmd=\
"docker run --rm --user $(id -u):$(id -g) --volume $(pwd):$(pwd) --workdir $(pwd) --entrypoint /bin/sh tymonx/cmake"

COPY --from=builder /usr/local/ /usr/local/

RUN apk add --quiet --no-cache \
        compiler-rt-static \
        musl-dev \
        binutils \
        doxygen \
        clang \
        ninja \
        perl \
        llvm \
        bash \
        make \
        git \
        gcc \
        g++
